Feature: User login

A user with a valid account inserts his login credentials correct and is logged into the system.

  Scenario: valid user logs in with invalid credentials
    Given a user is not authenticated and has a valid user account
    When the user logs in using invalid credentials
    Then the user should receive an error message stating he used invalid user credentials
    